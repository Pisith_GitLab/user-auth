// Connect to Mongodb via mongoose then export and inject in app.js
const mongoose = require('mongoose');
require('dotenv').config;

// const mongoURI = process.env.MONGO_URI;
const mongoURI = 'mongodb://localhost:27017/user_auth';
const connectDB = async () => {
    try {
        await mongoose.connect(mongoURI, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('MongoDB Connected...');
    } catch (err) {
        console.error(err.message);
        // Exit process with failure
        process.exit(1);
    }
}
module.exports = connectDB;
