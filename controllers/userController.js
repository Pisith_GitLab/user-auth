// Create { register, login } Controller for User Auth application using Express.js
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {

  // proper error handling
    if (!req.body) {
        res.status(400).json({message: 'No data provided'});
    }
    if (!req.body.email) {
        res.status(400).json({message: 'Email is required'});
    }
    if (!req.body.password) {
        res.status(400).json({message: 'Password is required'});
    }
    if (!req.body.username) {
        res.status(400).json({message: 'Username is required'});
    }

    const { username, email, password } = req.body;
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    try {
      const user = new User({
        username: username,
        email: email,
        password: hashedPassword
      });
      await user.save();
      res.status(201).json({message: `User Registerd Successfully`})
    } catch (err) {
        console.log(err);
        res.status(400).json(err);
    }
}

// create login controller
const login = async (req, res) => {
  if (!req.body) {
      return res.status(400).json({message: 'No data provided'});
  }
  if (!req.body.email) {
      return res.status(400).json({message: 'Email is required'});
  }
  if (!req.body.password) {
      return res.status(400).json({message: 'Password is required'});
  }

  const { email, password } = req.body;
  try {
      const user = await User.findOne({email: email});
      if (!user) {
          return res.status(400).json({message: 'User not found'});
      }
      if (!user.password) {
          return res.status(400).json({message: 'Password is not set for the user'});
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (!isMatch) {
          return res.status(400).json({message: 'Invalid Password'});
      }

       // Generate JWT token
      const token = jwt.sign({ userId: user._id }, 'SECRET_KEY', { expiresIn: '1h' });

      return res.status(200).json({message: `Login Successful`, token: token})
  } catch (err) {
      console.log(err);
      return res.status(400).json(err);
  }
}

// Implement change password controller
const changePassword = async (req, res) => {
  try {
    const {currentPassword , newPassword} = req.body;
    const userId = req.userId;

    const user = await User.findById(userId);
    // Check if user exists
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Verify current password
    const isMatch = await bcrypt.compare(currentPassword, user.password);
    if (!isMatch) {
      return res.status(400).json({ message: 'Invalid current password' });
    }

    // Generate new password hash
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(newPassword, salt); 

    // Update User's password
    user.password = hashedPassword;
    await user.save();

    res.status(200).json({ message: 'Password changed successfully' });
  } catch (error) {
    console.error(err);
    res.status(500).json({ message: 'Server error' });
  }
}


// export controllers
module.exports = {
    register,
    login,
    changePassword
};