// build express application
const express = require('express');
const bodyParser = require('body-parser');
const connectDB = require('./datasource/db');
const userRoute = require('./routes/userRoute');
const verifyToken = require('./middlewares/verify_token');
const cookieParser = require('cookie-parser');
const auditLogger = require('./middlewares/logger');

const app = express();
const PORT = process.env.PORT || 3000;

// use bodyParser middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

// Connect to DB
connectDB();

app.get('/', (req, res) => {
  res.status(200).json({message: `Hello Application is Running`})
})

// use userRoute
app.use('/api/users', auditLogger, userRoute);

// create protected route with verifyToken
app.get('/api/protected', verifyToken, (req, res) => {
  res.status(200).json({message: `Hello This is protected route`, userId: req.userId});
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
})