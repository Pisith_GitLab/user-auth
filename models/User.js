// Create User schema for User Authentication using Express.js and Mongoose
const mongoose = require('mongoose');

// Define Schema
const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

// Create user model
const User = mongoose.model('User', userSchema);

// export User
module.exports = User;
