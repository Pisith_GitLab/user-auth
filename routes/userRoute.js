// create User Routes using Express.js and userController.js
const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const verifyToken = require('../middlewares/verify_token');

// create routes
router.post('/register', userController.register);
router.post('/login', userController.login);
router.put('/change-password', verifyToken, userController.changePassword);

// export routes
module.exports = router;