const logger = require('../config/logger-config');

// Middleware for audit logging
function auditLogger(req, res, next) {
  // Log authentication event
  logger.info(`Authentication event: ${req.method} ${req.url}, User: ${req.userId}, Timestamp: ${new Date().toISOString()}`);
  next();
}

module.exports = auditLogger;