const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {

  // Get token from request headers, query parameters, or cookies
  const token = req.headers['authorization'] || req.query.token || req.cookies.token;

  // If no token, return 401
  if (!token) {
    return res.status(401).json({
      message: 'No token provided'
    });
  }

  jwt.verify(token, 'SECRET_KEY', (err, decoded) => {
    if (err) {
      return res.status(401).json({
        message: 'Unauthorized'
      });
    }
    req.decoded = decoded;
    req.userId = decoded.userId;
    next();
  })

}

// export
module.exports = verifyToken;